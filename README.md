# My personal site

Here is the code for http://francocedillo.com

I have used [Freelancer](http://startbootstrap.com/template-overviews/freelancer/) template by [Start Bootstrap](http://startbootstrap.com/).
Start Bootstrap was created by and is maintained by **David Miller**, Managing Parter at [Iron Summit Media Strategies](http://www.ironsummitmedia.com/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).
